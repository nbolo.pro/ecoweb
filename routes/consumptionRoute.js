const express = require('express')
const router = express.Router()
const passport = require('passport')
const consoService = require('../services/consumptionService')

router.post('/', passport.authenticate('jwt', {session: false}), async function (req, res) {
    const userId = req.user.userId
    const conso = req.body.conso
    let correctConso = isConsoCorrect(conso)

    if (correctConso.error) {
        res.status(400).json({errorMessage: correctConso.error})
    } else {
        try {
            console.log("User " + req.user.mail + " is trying to add a new consumption with value of " + conso)
            let consoObj = await consoService.addConsumption(userId, conso)
            if (consoObj) {
                res.status(201).json({consumption: consoObj.consumptionAmount})
            } else {
                res.status(409).json({errorMessage: 'User not found'})
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({errorMessage: 'Database error'})
        }
    }
})

/**
 * @param @notRequired month
 * @param @notRequired year
 * If the month and the year are not specified, returns all the data summed and ordered by month and year
 * If the month is specified but not the year it will take the current year
 * If the month and the year is specified, returns the data of the month and year
 * If the year is specified but not the month, returns the data of the year
 */
router.get('/history', passport.authenticate('jwt', {session: false}), async function (req, res) {
    let userId = req.user.userId
    let month = req.body.month
    let year = req.body.year
    try {
        let consoHistory = await consoService.getConsumptionHistory(userId, month, year)
        if (consoHistory.error) {
            res.status(409).json({errorMessage: consoHistory.error})
        } else if (consoHistory) {
            res.status(200).json({consumption: consoHistory})
        } else {
            res.status(500).json({errorMessage: 'Unexpected Error'})
        }
    } catch (e) {
        console.log(e)
        res.status(500).json({errorMessage: 'Database error'})
    }
})

function isConsoCorrect(conso) {
    let res = {}
    if (!conso) {
        res.error = 'conso not found in request.'
    } else if (isNaN(conso)) {
        res.error = 'conso should be a Double'
    } else if (conso < 0) {
        res.error = 'conso should be positive'
    }
    return res
}


module.exports = router
