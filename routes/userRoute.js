const express = require('express')
const userService = require('../services/userService.js')
const passport = require('passport')
const jwtUtils = require('../utils/jwtUtils')
const router = express.Router()


/**
 * Change the password of the connected user with a new one
 * @param oldPassword  ancien mot de passe
 * @param newPassword  nouveau mot de passe
 */
router.put('/password', passport.authenticate('jwt', {session: false}), async function (req, res) {
    const user = req.user
    const oldPassword = req.body.oldPassword
    const newPassword = req.body.newPassword

    if (!oldPassword || !newPassword || !user || !(await userService.verifyId(user.userId))) {
        res.status(400).json({errorMessage: 'OldPassword or newPassword not found.'})
    } else {
        try {
            console.log("User " + user.mail + " is trying to update her/his/it's password")
            if (await userService.updatePassword(user.userId, oldPassword, newPassword)) {
                res.status(202).json({successMessage: 'Password has been changed !'})
            } else {
                res.status(409).json({errorMessage: 'Old password does not match'})
            }
        } catch (e) {
            res.status(500).json({errorMessage: 'Database error. The password has not been changed'})
        }
    }
})


router.put('/', passport.authenticate('jwt', {session: false}), async function (req, res) {
    const userId = req.user.userId
    const name = req.body.name
    const surname = req.body.surname

    if (!name || !surname || !userId) {
        res.status(400).json({errorMessage: 'name, surname or user not found.'})
    } else {
        try {
            if (await userService.updateUser(userId, name, surname)) {
                let newToken = jwtUtils.generateToken(userId, name,surname,req.user.mail)
                res.status(202).json({token: newToken.token})
            } else {
                res.status(409).json({errorMessage: 'User not changed !'})
            }
        } catch (e) {
            console.log(e)
            res.status(500).json({errorMessage: 'Database error. The user has not been changed'})
        }
    }
})

module.exports = router
