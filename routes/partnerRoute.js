const express = require('express')
const router = express.Router()
const passport = require('passport')
const partnerService = require('../services/partnerService')

/**
 * List partners
 */
router.get('/', async function (req, res) {

    partnerService.listAll().then(partners => {
        res.status(200).json(partners)
    })
        .catch(err => {
            console.log(err)
            res.status(500).json({errorMessage: err})
        })

})

/**
 * Add partner
 */
router.post('/', passport.authenticate('jwt', {session: false}), async function (req, res) {

    const partnerName = req.body.partnerName
    const details = req.body.details

    if (!partnerName || !details) {
        res.status(400).json({errorMessage: "Missing partnerName and/or details"})
    } else {
        console.log("User " + req.user.mail + " is trying to add the partner: " + partnerName + "\nwith detail: " + details)

        partnerService.add(partnerName, details).then(partners => {
            res.status(200).json(partners)
        })
            .catch(err => {
                console.log(err)
                res.status(500).json({errorMessage: err})
            })
    }


})

module.exports = router
