const express = require('express')
const userService = require('../services/userService.js')
const passport = require('passport')
const jwtUtils = require('../utils/jwtUtils')
const router = express.Router()


router.post('/login', async function (req, res) {
    passport.authenticate(
        'basic',
        {session: false},
        (error, user) => {
            if (error || !user) {
                res.status(401).json({errorMessage: error})
            } else {

                let jwt = jwtUtils.generateToken(user.userId, user.name, user.surname, user.mail)

                /** assigns payload to req.user */
                req.login(jwt.payload, {session: false}, (error) => {
                    if (error) {
                        res.status(400).send({error})
                    } else {
                        res.status(200).json({
                            userId: user.userId,
                            token: jwt.token
                        })
                    }
                })
            }
        })(req, res)
})


router.post('/register', async function (req, res) {
    const mail = req.body.mail
    const password = req.body.password
    const name = req.body.name
    const surname = req.body.surname

    if (!mail || !password || !name || !surname) {
        res.status(400).json({errorMessage: 'Mail, password, name or surname not found in request.'})
    } else {
        try {
            console.log("User " + mail + " is trying to register with values :\nname: " + name + "\nsurname: " + surname)
            let user = await userService.register(mail, password, name, surname)
            if (user) {
                res.status(201).json({idUser: user.userId})
            } else {
                res.status(409).json({errorMessage: 'Mail already exists'})
            }
        } catch (e) {
            res.status(500).json({errorMessage: 'Database error'})
        }
    }
})

module.exports = router
