const express = require('express')
const router = express.Router()
const themeService = require('../services/themeService')

/**
 * List themes
 */
router.get('/', async function (req, res) {

    themeService.listAll().then(themes => {
        res.status(200).json(themes)
    })
        .catch(err => {
            console.log(err)
            res.status(500).json({errorMessage: err})
        })

})


module.exports = router
