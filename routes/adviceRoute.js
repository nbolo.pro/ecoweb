const express = require('express')
const router = express.Router()
const adviceService = require('../services/adviceService')

/**
 * Get all advices
 */
router.get('/', async function (req, res) {
    try {
        let advices = await adviceService.listAll()
        res.status(200).json(advices)
    } catch (e) {
        console.log(e)
        res.status(500).json({errorMessage: 'Database error'})
    }
})

module.exports = router
