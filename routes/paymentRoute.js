const express = require('express')
const paymentService = require('../services/paymentService')
const passport = require('passport')
const router = express.Router()

/**
 * Add payment to a user
 */
router.post('/', passport.authenticate('jwt', {session: false}), async function (req, res) {

    const user = req.user
    const amount = req.body.amount
    const partnerId = req.body.partnerId
    const refundedConsumption = req.body.refundedConsumption

    if (!amount || !partnerId || !user || !refundedConsumption || isNaN(refundedConsumption)) {
        res.status(400).json({errorMessage: 'Amount, PartnerId or refundedConsumption undefined'})
    } else {
        console.log("User " + user.mail + " is trying to pay partner with id " + partnerId + " with an amount of " + amount)
        paymentService.addPayment(user.userId, partnerId, amount, refundedConsumption).then(p => {
            res.status(201).json({paymentId: p.paymentId})
        })
            .catch(err => {
                console.log(err)
                res.status(500).json({errorMessage: 'Database error. The payment has not been added'})
            })
    }
})

/**
 * List payment of a user
 */
router.get('/', passport.authenticate('jwt', {session: false}), async function (req, res) {

    const user = req.user
    paymentService.listUserPayments(user.userId).then(p => {
        res.status(200).json(p)
    })
        .catch(err => {
            console.log(err)
            res.status(500).json({errorMessage: err})
        })

})


module.exports = router
