const createError = require('http-errors')
const express = require('express')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const indexRouter = require('./routes/index')
const authRouter = require('./routes/authRoute')
const consoRouter = require('./routes/consumptionRoute')
const adviceRouter = require('./routes/adviceRoute')
const userRouter = require('./routes/userRoute')
const paymentRouter = require('./routes/paymentRoute')
const partnerRouter = require('./routes/partnerRoute')
const themeRouter = require('./routes/themeRoute')
const app = express()

//load middleware for authentication
require('./passport.js')

//Force initialization of DB
// require('./services/database/init/dbInit.js')


// view engine setup
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(cookieParser())
app.use(cors())

//Applying routes to router
app.use('/api', indexRouter)
app.use('/api/auth', authRouter)
app.use('/api/consumptions', consoRouter)
app.use('/api/advices', adviceRouter)
app.use('/api/users', userRouter)
app.use('/api/payments', paymentRouter)
app.use('/api/partners', partnerRouter)
app.use('/api/themes', themeRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404))
})

// error handler (mostly 404)
app.use(function (err, req, res) {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // render the error page
    res.status(err.status || 500)
    res.json({
        errorMessage: err.message,
        errorStatus: err.status
    })
})

module.exports = app
