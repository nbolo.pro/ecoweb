const passport = require('passport');
const passportJWT = require('passport-jwt');
const BasicStrategy = require('passport-http').BasicStrategy;
const JWTStrategy = passportJWT.Strategy;
const userService = require('./services/userService.js')
const JWT_SECRET = require('./utils/constUtils.js').jwtsecret

//Defining Basic-Auth Strategy for login
passport.use(new BasicStrategy(
    async function(mail, password, done) {
        try {
            const user = await userService.authenticate(mail,password)
            if (user) {
                return done(null, user);
            } else {
                return done("Incorrect Mail / Password");
            }
        } catch (error) {
            console.log("Exception occurred during auth")
            console.log(error)
            return done(error);
        }
    }
  ));

//Defining JWT Strategy for restricted paths
passport.use(new JWTStrategy({
        //Taking token from Authorization: Bearer header
        jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: JWT_SECRET,
    },
    (jwtPayload, done) => {
        if (Date.now() > jwtPayload.expires) {
            return done('jwt expired');
        }
        return done(null, jwtPayload);
    }
));
