var exports = module.exports = {}
const partnerRepository = require('./database/repositories/partnerRepository')

exports.listAll = async function () {
    return await partnerRepository.listAll()
}

exports.add = async function (partnerName, details) {
    if (partnerName === "" || details === "") {
        throw new Error("partnerName or details empty")
    }
    return await partnerRepository.add(partnerName, details)
}
