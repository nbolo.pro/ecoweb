const userRepository = require('./database/repositories/userRepository')
const cryptoUtils = require('../utils/cryptoUtils.js')
var exports = module.exports = {}

/**
 * Authenticate a user : verifies mail and password exists and match database
 * @param mail
 * @param password
 * @returns {user}
 */
exports.authenticate = async function (mail, password) {
    try {
        console.log("'" + mail + "' is trying to connect.")
        const user = await userRepository.findByMail(mail)
        if (user && cryptoUtils.checkPwd(password, user.password)) {
            console.log("Credentials of '" + mail + "' are corrects.")
            console.log(user.dataValues)
            return user
        }
        console.log('Credentials of ' + mail + ' are incorrect.')
        return false
    } catch (err) {
        console.log("Error during pseudo verification of '" + mail + "' : " + err)
        throw err
    }

}

/**
 * Register a new user and save it in the databse
 * @returns an instance of class User defined in userModel.js
 */
exports.register = async function (mail, password, name, surname) {
    console.log("New user '" + mail + "' is trying to register.")
    try {
        if (await userRepository.findByMail(mail)) {
            console.log('Mail is already used')
            return undefined
        }
    } catch (e) {
        console.log('Error during verification of doublons : ' + e)
        throw e
    }
    try {
        const res = await userRepository.addUser(mail, cryptoUtils.hashPwd(password), name, surname)
        if (res) {
            console.log("User '" + mail + "' has been added successfully.")
            return res
        } else {
            throw new Error("An error has occurred on the result of the database")
        }
    } catch (err) {
        console.log("Error adding of '" + mail + "' : " + err)
        throw err
    }
}

/**
 * Update the password of an existing user and verifies
 * that the old password corresponds
 * @param userId
 * @param oldPassword
 * @param newPassword
 * @returns {Promise<boolean>}
 */
exports.updatePassword = async function (userId, oldPassword, newPassword) {
    console.log("Updating password of user '" + userId + "'")
    try {
        const user = await userRepository.findById(userId)
        if (user && cryptoUtils.checkPwd(oldPassword, user.password)) {
            await userRepository.updatePassword(user.userId, cryptoUtils.hashPwd(newPassword))
            return true
        }
        return false
    } catch (e) {
        console.log('Error during update of user : ' + e)
        throw e
    }
}

exports.updateUser = async function (userId, name, surname) {
    console.log("Updating parameters of user '" + userId + "'")
    try {
        return await userRepository.updateUser(userId, name, surname)
    } catch (e) {
        console.log('Error during update of user : ' + e)
        throw e
    }
}

/**
 * Verify the existence of a user
 */
exports.verifyId = async function (userId) {
    return userRepository.findById(userId).then(u => {
        return true
    })
        .catch(e => {
            throw e
        })
}


