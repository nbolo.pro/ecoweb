var exports = module.exports = {}
const themeRepository = require('./database/repositories/themeRepository')

exports.listAll = async function(){
    return await themeRepository.findAll()
}
