const paymentRepository = require('./database/repositories/paymentRepository')
const userRepository = require('./database/repositories/userRepository')
const partnerRepository = require('./database/repositories/partnerRepository')
var exports = module.exports = {}

/**
 * Add a new Payment
 * @param userId
 * @param partnerId
 * @param amount
 * @returns {Promise{Payment}}
 */
exports.addPayment = async function(userId,  partnerId, amount, refundedConsumption){

    const user = userRepository.findById(userId)
    const partner = partnerRepository.findById(partnerId)

    let paymentDTO = {
        amount : amount,
        refundedConsumption: refundedConsumption
    }

    return await Promise.all([user, partner]).then(res => {
        paymentDTO.paymentDate = Date.now()
        return paymentRepository.addPayment(res[0].userId, res[1].partnerId, paymentDTO)
    })
    .catch(err => {
        console.log('Error during insert of payment')
        throw err
    })
}

exports.listUserPayments = async function(userId){
    return userRepository.findById(userId).then(u => {
        return paymentRepository.getUserPayments(u.userId)
    })
    .catch(err =>{
        console.log('User not found')
        throw err
    })
}
