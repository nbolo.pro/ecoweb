const db = require('../connexion')
var exports = module.exports = {}

exports.init = async function () {
    return await Promise.all([
        db.Partner.findOrCreate({
            where: {partnerName: 'WWF (World Wide Fund for Nature)'},
            defaults: {
                partnerName: 'WWF (World Wide Fund for Nature)',
                details: "Le WWF (de l'anglais : World Wildlife Fund) ou Fonds mondial pour la nature" +
                    "est une organisation non gouvernementale internationale (ONGI) créée en 1961," +
                    " vouée à la protection de l'environnement et au développement durable." +
                    "C'est l'une des plus importantes ONGI environnementalistes du monde avec plus " +
                    "de six millions de soutiens à travers le monde, travaillant dans plus de cent pays," +
                    " et soutenant environ 1 300 projets environnementaux.",
                logoURL: 'https://cdn.futura-sciences.com/buildsv6/images/largeoriginal/e/0/0/e001be99d2_50019095_logo-wwf-reference.jpg'
            }
        }),

        db.Partner.findOrCreate({
            where: {partnerName: 'GreenPeace'},
            defaults: {
                partnerName: 'GreenPeace',
                details: 'Greenpeace est une organisation non ' +
                    'gouvernementale internationale (ONGI) de protection de l\'environnement' +
                    ' présente dans plus de 55 pays à travers le monde. ' +
                    'L’organisation Greenpeace est un groupe de plaidoyer luttant contre ce ' +
                    'qu\'elle estime être les plus grandes menaces pour l\'environnement ' +
                    'et la biodiversité sur la planète.',
                logoURL: 'https://dn9ly4f9mxjxv.cloudfront.net/app/uploads/2019/03/xgreenpeace1-300x300.png.pagespeed.ic.h7HanwSuz2.png'
            }
        }),

        db.Partner.findOrCreate({
            where: {partnerName: 'Sea Shepherd Conservation Society'},
            defaults: {
                partnerName: 'Sea Shepherd Conservation Society',
                details: 'La Sea Shepherd Conservation Society (SSCS) est une organisation' +
                    ' non gouvernementale internationale maritime à but non lucratif,' +
                    ' vouée à la protection des écosystèmes marins et de la biodiversité.' +
                    ' Elle est particulièrement engagée dans la lutte contre la pêche illégale,' +
                    ' la chasse à la baleine, la chasse aux dauphins au Japon, la chasse aux' +
                    ' globicéphales aux îles Féroé, la chasse aux phoques et la surpêche liée' +
                    ' à la pêche industrielle.',
                logoURL: 'https://is4-ssl.mzstatic.com/image/thumb/Purple118/v4/b4/48/af/b448af48-ce94-d205-d0e8-b92cada3fc57/AppIcon-1x_U007emarketing-85-220-3.png/246x0w.png'
            }
        }),

        db.Partner.findOrCreate({
            where: {partnerName: 'Union internationale pour la conservation de la nature'},
            defaults: {
                partnerName: 'Union internationale pour la conservation de la nature',
                details: 'L\'Union internationale pour la conservation de la nature (UICN, en anglais IUCN)' +
                    ' est l\'une des principales organisations non gouvernementales mondiales consacrées' +
                    ' à la conservation de la nature. Sa mission est d\'influencer, d\'encourager et' +
                    ' d\'assister les sociétés du monde entier, dans la conservation de l\'intégrité et de' +
                    ' la biodiversité de la nature, ainsi que de s\'assurer que l\'utilisation des ressources' +
                    ' naturelles est faite de façon équitable et durable.',
                logoURL: 'https://www.iucn.org/sites/dev/files/iucn_logo_twcard.png'
            }
        }),

        db.Partner.findOrCreate({
            where: {partnerName: 'Rainforest Alliance'},
            defaults: {
                partnerName: 'Rainforest Alliance',
                details: 'Rainforest Alliance est une ONG américaine qui a pour objectif de' +
                    ' préserver la biodiversité et la durabilité. Cette organisation travaille' +
                    ' avec des gens dont les moyens de subsistance dépendent de la terre,' +
                    ' les aidant à transformer' +
                    ' leur méthode de culture, de récolte du bois ou encore d\'accueil des voyageurs.',
                logoURL: 'https://www.labelinfo.be/sites/default/files/label/rainforest_alliance_certified_0.png'
            }
        }),

        db.Partner.findOrCreate({
            where: {partnerName: 'Green Cross International'},
            defaults: {
                partnerName: 'Green Cross International',
                details: 'Green Cross International, ou Croix verte internationale,' +
                    ' est une organisation non gouvernementale internationale à but environnemental.' +
                    ' Elle se donne pour mission de contribuer à l\'augmentation du niveau' +
                    ' de vie et du développement économique et social dans tous les pays,' +
                    ' développés ou en voie de développement où c\'est possible.' +
                    ' Elle met pour cela en place des projets divers concernant l\'environnement,' +
                    ' des processus de paix ou d\'aide au développement. Ainsi Green Cross a installé' +
                    ' des camps thérapeutiques près du site de la catastrophe nucléaire de Tchernobyl' +
                    ' ou a lancé des campagnes de prévention de conflits liés aux ressources en eau',
                logoURL: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRKcwChxSQxado6UfGJQa5ebUzRcpf5vEObZn2NUWkRX-1AgO9n'
            }
        })
    ])
}
