const db = require('../connexion')
var exports = module.exports = {}


exports.init = async function () {

    let themes = await db.Theme.findAll()

    return await Promise.all([
        db.Advice.findOrCreate({
            where: {title: "Désabonnez-vous des newsletters inutiles"},
            defaults: {
                content: "Vous recevez certainement pas mal de newsletter dans vos mails dont certaines que vous n’avez jamais demandé. C’est notre cas à tous, un mauvais clic, un mail de pub, nous en avons tous déjà eu dans notre boite mail. Cependant, savez-vous que les serveurs qui envoient ces mails ont besoin de refroidir ? Qui dit refroidir d’un côté dit réchauffer de l’autre. C’est donc souvent l'eau des mers et tout leur écosystèmes qui prennent pour quelques dizaines de milliers de mails."
            }
        }).then(([advice, created]) => {
            advice.setThemes([themes[1], themes[5], themes[7]])
        }),
        db.Advice.findOrCreate({
            where: {title: "Accèdez directement à vos sites favoris"},
            defaults: {
                content: "Nous avons tous nos petites habitudes sur internet et nous visitons souvent les mêmes sites internet. Un geste très simple permet de reduire l'empreinte carbone de votre navigation: enregistrer ses sites préférés en favori. Cela permet d'éviter une recherche, et malgré le gain insignifiant sur une recherche, multiplié par le nombre de recherche qui sont faites quotidiennement cela fait un gain énorme."
            }
        }).then(([advice, created]) => {
            advice.setThemes([themes[2], themes[7]])
        }),
        db.Advice.findOrCreate({
            where: {title: "Je vide la corbeille de ma boite mail"},
            defaults: {
                content: "Notre boîte mail est le parfait exemple de l’impact négatif sur la planète que notre vie technologique peut engendrer “à l’insu de notre plein gré”. Et oui, il ne faut pas se voiler la face, si on nous pose la question, on sait que les serveurs consomment énormément d’électricité. Par contre, au moment de faire le lien entre corbeille pleine, données qui ne servent à rien, serveurs pleins à craquer qui tournent inutilement et gâchis de ressources naturelles, là, c’est plus compliqué. Prennez le réflexe vider la corbeille de votre boite mail. Plus d'informations: https://cacommenceparmoi.org/action/vider-sa-corbeille/"
            }
        }).then(([advice, created]) => {
            advice.setThemes([themes[1], themes[5], themes[7]])
        }),
        db.Advice.findOrCreate({
            where: {title: "Je désactive l'autoplay des vidéos sur les réseaux sociaux et sites de VOD"},
            defaults: {
                content: "La vidéo, sous toute ses formes, représentent près de 80% du traffic internet mondial. Sur les plateformes et réseaux sociaux, les vidéos sont souvent lancées de manière automatique (ce que l'on appelle autoplay). Choisissons le contenu vidéo que l'on souhaite regarder plutôt que de laisser les plateformes nous imposer la lecture automatique. Pour cela, la majorité des plateformes permettent de désactiver cet \"autoplay\" dans les paramètres, afin de réduire considérablement la consommation totale de notre navigation. Plus d'informations : https://theshiftproject.org/article/climat-insoutenable-usage-video/"
            }
        }).then(([advice, created]) => {
            advice.setThemes([themes[0], themes[2], themes[5]])
        }),
        db.Advice.findOrCreate({
            where: {title: "Je navigue en priorité sur des connexions wifi ou filaires, en évitant le réseau mobile"},
            defaults: {
                content: "Selon greenit.fr, la navigation sur un réseau 4G consommerait jusqu'à 23 fois plus d'énergies que sur un réseau wifi. Par conséquent, j'évite les téléchargements et la consommation de vidéos en ligne lorsque je suis connecté via un réseau mobile (3G, 4G, bientôt 5G). Plus d'informations : https://www.greenit.fr/2016/03/15/internet-mobile-la-4g-est-elle-une-abomination-energetique/"
            }
        }).then(([advice, created]) => {
            advice.setThemes([themes[2], themes[4], themes[6], themes[7]])
        }),
        db.Advice.findOrCreate({
            where: {title: "J'utilise un moteur de recherche éco-responsable"},
            defaults: {
                content: "Il existe plusieurs moteurs de recherche qui utilisent les revenus générés par la publicité pour rendre le monde plus vert. En choisissant par exemple d’installer l’extension du métamoteur Lilo sur votre navigateur, vous transformez chaque recherche en micro-don destiné à des associations ou des entreprises solidaires. Quant à Ecosia, il reverse 80 % de ses revenus à un programme de plantation d’arbres au Brésil."
            }
        }).then(([advice, created]) => {
            advice.setThemes([themes[2], themes[7]])
        }),
        db.Advice.findOrCreate({
            where: {title: "J'utilise la version sombre/nuit des sites s'ils le proposent"},
            defaults: {
                content: "La mode du moment est d'utiliser le mode sombre/nuit des sites, cela ne permet pas seulement de reposer vos petits yeux, en effet, votre ordinateur va consommer plus d'énergie à vouloir afficher du blanc plutôt que du noir. Pour plus d'informations : https://rotek.fr/mode-sombre-pourquoi-il-faut-lutiliser/"
            }
        }).then(([advice, created]) => {
            advice.setThemes([themes[2], themes[7]])
        }),
        db.Advice.findOrCreate({
            where: {title: "J'empêche les publicités de polluer mon navigateur"},
            defaults: {
                content: "Comme n’importe quel contenu sur la toile, pour s’afficher, la publicité (surtout vidéo) consomme de l’énergie à travers les serveurs. Utiliser un bloqueur de publicités – par exemple Adblock et Ghostery – pour se séparer ces contenus énergivores vous évitera bon nombres de pages non voulues. Attention cependant, certains navigateurs éco-responsables tels que Ecosia ont besoin de la publicité pour financer leurs cause, nous vous recommendons donc de le désactiver dessus."
            }
        }).then(([advice, created]) => {
            advice.setThemes([themes[2], themes[7], themes[0], themes[6]])
        }),
        db.Advice.findOrCreate({
            where: {title: "J'utilise une clef USB plutôt que d'envoyer un fichier lourd par mail"},
            defaults: {
                content: "Vous avez des documents, des images ou des vidéos à partager avec un ou une collègue ? Plutôt que d’envoyer un lourd courriel, favorisez le contact humain et aller le lui porter en main propre sur une clef USB. Si la personne n’est pas présente physiquement, privilégiez l’envoi de pièces jointes par des services tel Wetransfer, qui ne conserve les données que quelques jours sur les serveurs avant de les supprimer."
            }
        }).then(([advice, created]) => {
            advice.setThemes([themes[0], themes[1], themes[3], themes[4], themes[7]])
        }),
        db.Advice.findOrCreate({
            where: {title: "J'utilise une police de caractère écolo"},
            defaults: {
                content: "Enfin voici une astuce qui dépasse le cadre de l’utilisation d’Internet : si vous avez vraiment besoin d’imprimer un document, passez-le en police Ecofont, qui utilise moins d’encre que toutes les autres, car chaque caractère est criblé de petits points blancs. Lien pour EcoFont : https://ecofont.fr.uptodown.com/windows"
            }
        }).then(([advice, created]) => {
            advice.setThemes([themes[8]])
        }),
    ])

}
