const db = require('../connexion')
var exports = module.exports = {}

exports.init = async function () {
    return await Promise.all([
        db.Payment.findOrCreate({
            where: {
                userId: 1,
                partnerId: 1
            },
            defaults: {
                amount: 30.20,
                paymentDate: Date.now(),
                refundedConsumption: 3.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 1,
                partnerId: 2
            },
            defaults: {
                amount: 62.20,
                paymentDate: Date.now(),
                refundedConsumption: 20.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 1,
                partnerId: 3
            },
            defaults: {
                amount: 14.03,
                paymentDate: Date.now(),
                refundedConsumption: 1.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 1,
                partnerId: 4
            },
            defaults: {
                amount: 120.20,
                paymentDate: Date.now(),
                refundedConsumption: 160.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 1,
                partnerId: 5
            },
            defaults: {
                amount: 12.20,
                paymentDate: Date.now(),
                refundedConsumption: 5.26
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 2,
                partnerId: 1
            },
            defaults: {
                amount: 30.20,
                paymentDate: Date.now(),
                refundedConsumption: 3.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 2,
                partnerId: 2
            },
            defaults: {
                amount: 62.20,
                paymentDate: Date.now(),
                refundedConsumption: 20.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 2,
                partnerId: 3
            },
            defaults: {
                amount: 14.03,
                paymentDate: Date.now(),
                refundedConsumption: 1.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 2,
                partnerId: 4
            },
            defaults: {
                amount: 120.20,
                paymentDate: Date.now(),
                refundedConsumption: 160.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 3,
                partnerId: 1
            },
            defaults: {
                amount: 30.20,
                paymentDate: Date.now(),
                refundedConsumption: 3.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 3,
                partnerId: 2
            },
            defaults: {
                amount: 62.20,
                paymentDate: Date.now(),
                refundedConsumption: 20.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 3,
                partnerId: 3
            },
            defaults: {
                amount: 14.03,
                paymentDate: Date.now(),
                refundedConsumption: 1.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 3,
                partnerId: 4
            },
            defaults: {
                amount: 120.20,
                paymentDate: Date.now(),
                refundedConsumption: 160.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 4,
                partnerId: 1
            },
            defaults: {
                amount: 30.20,
                paymentDate: Date.now(),
                refundedConsumption: 3.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 4,
                partnerId: 2
            },
            defaults: {
                amount: 62.20,
                paymentDate: Date.now(),
                refundedConsumption: 20.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 4,
                partnerId: 3
            },
            defaults: {
                amount: 14.03,
                paymentDate: Date.now(),
                refundedConsumption: 1.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 4,
                partnerId: 4
            },
            defaults: {
                amount: 120.20,
                paymentDate: Date.now(),
                refundedConsumption: 160.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 5,
                partnerId: 1
            },
            defaults: {
                amount: 30.20,
                paymentDate: Date.now(),
                refundedConsumption: 3.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 5,
                partnerId: 2
            },
            defaults: {
                amount: 62.20,
                paymentDate: Date.now(),
                refundedConsumption: 20.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 5,
                partnerId: 3
            },
            defaults: {
                amount: 14.03,
                paymentDate: Date.now(),
                refundedConsumption: 1.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 5,
                partnerId: 4
            },
            defaults: {
                amount: 120.20,
                paymentDate: Date.now(),
                refundedConsumption: 160.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 6,
                partnerId: 1
            },
            defaults: {
                amount: 30.20,
                paymentDate: Date.now(),
                refundedConsumption: 3.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 6,
                partnerId: 2
            },
            defaults: {
                amount: 62.20,
                paymentDate: Date.now(),
                refundedConsumption: 20.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 6,
                partnerId: 3
            },
            defaults: {
                amount: 14.03,
                paymentDate: Date.now(),
                refundedConsumption: 1.06
            }
        }),
        db.Payment.findOrCreate({
            where: {
                userId: 6,
                partnerId: 4
            },
            defaults: {
                amount: 120.20,
                paymentDate: Date.now(),
                refundedConsumption: 160.06
            }
        })
    ])
}
