const db = require('../connexion')
const themes = require('./themeInit.js')
const advices = require('./advicesInit.js')
const users = require('./userInit.js')
const consos = require('./consoInit')
const payments = require('./paymentInit')
const partners = require('./partnerInit')
// Persiste les changements en base

// A décommenter pour modifier le schéma de base de donnée
// ATTENTION : cela supprime les tables et donc les données
db.sequelize.sync({force: true}).then(() => {
    users.init().then(() => {
        console.log('Users inserted successfully')
        themes.init().then(() => {
            console.log('Themes inserted successfully')
            advices.init().then(() =>
                console.log('Advices inserted successfully')
            )
        })
        consos.init().then(() => console.log('Consumptions inserted successfully'))

        partners.init().then(() => {
            console.log('Partners inserted successfully')
            payments.init().then(() => console.log('Payments inserted successfully'))
        })
    })


})
