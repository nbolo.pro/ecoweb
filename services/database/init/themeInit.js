const db = require('../connexion')
var exports = module.exports = {}

exports.init = async function () {
    return await Promise.all([
        db.Theme.findOrCreate({
            where : {themeName: 'Vidéo'},
            defaults: {
                themeName: 'Vidéo',
                themeColor: '#3498db'
            }
        }),
        db.Theme.findOrCreate({
            where : {themeName: 'Mail'},
            defaults: {
                themeName: 'Mail',
                themeColor: '#34495e'
            }
        }),
        db.Theme.findOrCreate({
            where : {themeName: 'Navigation'},
            defaults: {
                themeName: 'Navigation',
                themeColor: '#1abc9c'
            }
        }),
        db.Theme.findOrCreate({
            where : {themeName: 'Musique'},
            defaults: {
                themeName: 'Musique',
                themeColor: '#f1c40f'
            }
        }),
        db.Theme.findOrCreate({
            where : {themeName: 'Téléchargement'},
            defaults: {
                themeName: 'Téléchargement',
                themeColor: '#e67e22'
            }
        }),
        db.Theme.findOrCreate({
            where : {themeName: 'Gestion'},
            defaults: {
                themeName: 'Gestion',
                themeColor: '#9b59b6'
            }
        }),
        db.Theme.findOrCreate({
            where : {themeName: 'Réseau'},
            defaults: {
                themeName: 'Réseau',
                themeColor: '#e74c3c'
            }
        }),
        db.Theme.findOrCreate({
            where : {themeName: 'Habitude'},
            defaults: {
                themeName: 'Habitude',
                themeColor: '#2ecc71'
            }
        }),
        db.Theme.findOrCreate({
            where : {themeName: 'Autre'},
            defaults: {
                themeName: 'Autre',
                themeColor: '#95a5a6'
            }
        })
    ])
}







