const db = require('../connexion')
var exports = module.exports = {}

exports.init = async function () {

    return await Promise.all([

        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 1
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 0.03,
                userId: 1
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 0.03,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 1.33,
                userId: 1
            },
            defaults: {
                consumptionDate: new Date(2020, 0, 3),
                consumptionAmount: 1.33,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 1,
                consumptionDate: new Date(2019, 11, 14)
            },
            defaults: {
                consumptionDate: new Date(2019, 11, 14),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 36.11,
                consumptionDate: new Date(2019, 0, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 59.36,
                consumptionDate: new Date(2019, 1, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 301.25,
                consumptionDate: new Date(2019, 2, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 140.02,
                consumptionDate: new Date(2019, 3, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 69.69,
                consumptionDate: new Date(2019, 4, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 5, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 150.02,
                consumptionDate: new Date(2019, 6, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 64.02,
                consumptionDate: new Date(2019, 7, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 320.02,
                consumptionDate: new Date(2019, 8, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 560.02,
                consumptionDate: new Date(2019, 9, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 310.02,
                consumptionDate: new Date(2019, 10, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 11, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 87.02,
                consumptionDate: new Date(2020, 0, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 210.02,
                consumptionDate: new Date(2020, 1, 19),
                userId: 1
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 21.02,
                consumptionDate: new Date(2020, 2, 19),
                userId: 1
            }
        }),

        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 2
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 0.03,
                userId: 2
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 0.03,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 1.33,
                userId: 2
            },
            defaults: {
                consumptionDate: new Date(2020, 0, 3),
                consumptionAmount: 1.33,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 2,
                consumptionDate: new Date(2019, 11, 14)
            },
            defaults: {
                consumptionDate: new Date(2019, 11, 14),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 36.11,
                consumptionDate: new Date(2019, 0, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 59.36,
                consumptionDate: new Date(2019, 1, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 301.25,
                consumptionDate: new Date(2019, 2, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 140.02,
                consumptionDate: new Date(2019, 3, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 69.69,
                consumptionDate: new Date(2019, 4, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 5, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 150.02,
                consumptionDate: new Date(2019, 6, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 64.02,
                consumptionDate: new Date(2019, 7, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 320.02,
                consumptionDate: new Date(2019, 8, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 560.02,
                consumptionDate: new Date(2019, 9, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 310.02,
                consumptionDate: new Date(2019, 10, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 11, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 87.02,
                consumptionDate: new Date(2020, 0, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 210.02,
                consumptionDate: new Date(2020, 1, 19),
                userId: 2
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 21.02,
                consumptionDate: new Date(2020, 2, 19),
                userId: 2
            }
        }),


        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 3
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 0.03,
                userId: 3
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 0.03,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 1.33,
                userId: 3
            },
            defaults: {
                consumptionDate: new Date(2020, 0, 3),
                consumptionAmount: 1.33,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 3,
                consumptionDate: new Date(2019, 11, 14)
            },
            defaults: {
                consumptionDate: new Date(2019, 11, 14),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 36.11,
                consumptionDate: new Date(2019, 0, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 59.36,
                consumptionDate: new Date(2019, 1, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 301.25,
                consumptionDate: new Date(2019, 2, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 140.02,
                consumptionDate: new Date(2019, 3, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 69.69,
                consumptionDate: new Date(2019, 4, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 5, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 150.02,
                consumptionDate: new Date(2019, 6, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 64.02,
                consumptionDate: new Date(2019, 7, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 320.02,
                consumptionDate: new Date(2019, 8, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 560.02,
                consumptionDate: new Date(2019, 9, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 310.02,
                consumptionDate: new Date(2019, 10, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 11, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 87.02,
                consumptionDate: new Date(2020, 0, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 210.02,
                consumptionDate: new Date(2020, 1, 19),
                userId: 3
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 21.02,
                consumptionDate: new Date(2020, 2, 19),
                userId: 3
            }
        }),


        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 4
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 0.03,
                userId: 4
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 0.03,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 1.33,
                userId: 4
            },
            defaults: {
                consumptionDate: new Date(2020, 0, 3),
                consumptionAmount: 1.33,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 4,
                consumptionDate: new Date(2019, 11, 14)
            },
            defaults: {
                consumptionDate: new Date(2019, 11, 14),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 36.11,
                consumptionDate: new Date(2019, 0, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 59.36,
                consumptionDate: new Date(2019, 1, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 301.25,
                consumptionDate: new Date(2019, 2, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 140.02,
                consumptionDate: new Date(2019, 3, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 69.69,
                consumptionDate: new Date(2019, 4, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 5, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 150.02,
                consumptionDate: new Date(2019, 6, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 64.02,
                consumptionDate: new Date(2019, 7, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 320.02,
                consumptionDate: new Date(2019, 8, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 560.02,
                consumptionDate: new Date(2019, 9, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 310.02,
                consumptionDate: new Date(2019, 10, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 11, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 87.02,
                consumptionDate: new Date(2020, 0, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 210.02,
                consumptionDate: new Date(2020, 1, 19),
                userId: 4
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 21.02,
                consumptionDate: new Date(2020, 2, 19),
                userId: 4
            }
        }),


        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 5
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 0.03,
                userId: 5
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 0.03,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 1.33,
                userId: 5
            },
            defaults: {
                consumptionDate: new Date(2020, 0, 3),
                consumptionAmount: 1.33,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 5,
                consumptionDate: new Date(2019, 11, 14)
            },
            defaults: {
                consumptionDate: new Date(2019, 11, 14),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 36.11,
                consumptionDate: new Date(2019, 0, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 59.36,
                consumptionDate: new Date(2019, 1, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 301.25,
                consumptionDate: new Date(2019, 2, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 140.02,
                consumptionDate: new Date(2019, 3, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 69.69,
                consumptionDate: new Date(2019, 4, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 5, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 150.02,
                consumptionDate: new Date(2019, 6, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 64.02,
                consumptionDate: new Date(2019, 7, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 320.02,
                consumptionDate: new Date(2019, 8, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 560.02,
                consumptionDate: new Date(2019, 9, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 310.02,
                consumptionDate: new Date(2019, 10, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 11, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 87.02,
                consumptionDate: new Date(2020, 0, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 210.02,
                consumptionDate: new Date(2020, 1, 19),
                userId: 5
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 21.02,
                consumptionDate: new Date(2020, 2, 19),
                userId: 5
            }
        }),


        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 6
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 0.03,
                userId: 6
            },
            defaults: {
                consumptionDate: new Date(),
                consumptionAmount: 0.03,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 1.33,
                userId: 6
            },
            defaults: {
                consumptionDate: new Date(2020, 0, 3),
                consumptionAmount: 1.33,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 3.02,
                userId: 6,
                consumptionDate: new Date(2019, 11, 14)
            },
            defaults: {
                consumptionDate: new Date(2019, 11, 14),
                consumptionAmount: 3.02,
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 36.11,
                consumptionDate: new Date(2019, 0, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 59.36,
                consumptionDate: new Date(2019, 1, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 301.25,
                consumptionDate: new Date(2019, 2, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 140.02,
                consumptionDate: new Date(2019, 3, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 69.69,
                consumptionDate: new Date(2019, 4, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 5, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 150.02,
                consumptionDate: new Date(2019, 6, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 64.02,
                consumptionDate: new Date(2019, 7, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 320.02,
                consumptionDate: new Date(2019, 8, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 560.02,
                consumptionDate: new Date(2019, 9, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 310.02,
                consumptionDate: new Date(2019, 10, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 178.02,
                consumptionDate: new Date(2019, 11, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 87.02,
                consumptionDate: new Date(2020, 0, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 210.02,
                consumptionDate: new Date(2020, 1, 19),
                userId: 6
            }
        }),
        db.Consumption.findOrCreate({
            where: {
                consumptionAmount: 21.02,
                consumptionDate: new Date(2020, 2, 19),
                userId: 6
            }
        }),

    ])
}

