var exports = module.exports = {}
const db = require('../connexion')

exports.findAll = async function(){
    return db.Advice.findAll({
        attributes: ['adviceId','title', 'content','createdAt'],
        include: {
            model: db.Theme,
            attributes: ['themeId','themeColor','themeName'],
            through: {attributes: []}
        }
    })
}
