const db = require('../connexion')
var exports = module.exports = {}

/**
 * Insert a new payment in the database
 * @param userId
 * @param partnerId
 * @param paymentInfo
 * @returns {Payment}
 */
exports.addPayment = async function(userId, partnerId, paymentInfo){
    return db.Payment.create({
        amount : paymentInfo.amount,
        paymentDate : paymentInfo.paymentDate,
        refundedConsumption: paymentInfo.refundedConsumption,
        partnerId : partnerId,
        userId : userId
    })
}


exports.getUserPayments = async function(userId){
    return db.Payment.findAll({
        where : {userId: userId},
        attributes : ['paymentId', 'paymentDate', 'amount', 'refundedConsumption'],
        include: [{
            model: db.Partner,
            attributes : ['partnerName', 'details', 'logoURL']
        }]
    })
}

