var exports = module.exports = {}
const db = require('../connexion')

exports.findById = async function(partnerId) {
    return db.Partner.findByPk(partnerId)
}

exports.listAll = async function(){
    return db.Partner.findAll()
}

exports.add = async function(partnerName, details){
    return db.Partner.create({
        partnerName : partnerName,
        details : details
    })
}
