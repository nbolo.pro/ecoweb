const db = require('../connexion')
var exports = module.exports = {}

exports.findByMail = async function (mail) {
    return db.User.findOne({
        where: {
            mail: mail
        }
    })
}

exports.addUser = async function (mail,password,name,surname) {
    return db.User.create({
        mail: mail,
        password: password,
        name: name,
        surname : surname
    });
}

exports.updatePassword = async function (userId, newPassword) {
    return db.User.update(
        { password: newPassword },
        { where: { userId: userId } });
}

exports.updateUser = async function (userId, name, surname) {
    return db.User.update(
        {
            name: name,
            surname: surname
        },
        { where: { userId: userId } });
}

exports.findById = async function(userId){
    return db.User.findByPk(userId)
}
