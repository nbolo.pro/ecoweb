var exports = module.exports = {}
const Sequelize = require('sequelize')
const db = require('../connexion')
const Op = Sequelize.Op

exports.addConsumption = async function (timestamp, conso, userId) {
    return db.Consumption.create({
        consumptionDate: timestamp,
        consumptionAmount: conso,
        userId: userId
    })
}

exports.getConsumptionByMonth = async function (userId, month) {
    const currentYear = new Date().getFullYear()
    return this.getConsumptionByMonthAndYear(userId, month, currentYear)
}

exports.getConsumptionByYear = async function (userId, year) {
    return db.Consumption.findAll({
        attributes: [
            [db.sequelize.fn('date_part', 'month', db.sequelize.col('consumptionDate')), 'consumptionMonth'],
            [db.sequelize.fn('date_part', 'year', db.sequelize.col('consumptionDate')), 'consumptionYear'],
            [db.sequelize.fn('ROUND', db.sequelize.cast(db.sequelize.fn('SUM', db.sequelize.col('consumptionAmount')),'NUMERIC'), 2), 'consumption']
        ],
        where: {
            userId: userId,
            consumptionDate: {
                [Op.gte]: new Date(year, 0, 1),
                [Op.lte]: new Date(year, 11, 31)
            }
        },
        group: ['consumptionMonth', 'consumptionYear']
    })
}

exports.getConsumptionByMonthAndYear = async function (userId, month, year) {
    return db.Consumption.findAll({
        attributes: [
            [db.sequelize.fn('date_part', 'month', db.sequelize.col('consumptionDate')), 'consumptionMonth'],
            [db.sequelize.fn('date_part', 'year', db.sequelize.col('consumptionDate')), 'consumptionYear'],
            [db.sequelize.fn('ROUND', db.sequelize.cast(db.sequelize.fn('SUM', db.sequelize.col('consumptionAmount')),'NUMERIC'), 2), 'consumption']
        ],
        where: {
            userId: userId,
            consumptionDate: {
                [Op.gte]: new Date(year, (month - 1), 1),
                [Op.lte]: new Date(year, (month - 1), 31)
            }
        },
        group: ['consumptionMonth', 'consumptionYear']
    })
}

exports.getConsumption = async function (userId) {
    return db.Consumption.findAll({
        attributes: [
            [db.sequelize.fn('date_part', 'month', db.sequelize.col('consumptionDate')), 'consumptionMonth'],
            [db.sequelize.fn('date_part', 'year', db.sequelize.col('consumptionDate')), 'consumptionYear'],
            [db.sequelize.fn('ROUND', db.sequelize.cast(db.sequelize.fn('SUM', db.sequelize.col('consumptionAmount')),'NUMERIC'), 2), 'consumption']
        ],
        where: {userId: userId},
        group: ['consumptionMonth', 'consumptionYear']
    })
}
