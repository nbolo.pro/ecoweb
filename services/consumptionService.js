const consoRepository = require('./database/repositories/consumptionRepository')
var exports = module.exports = {}

exports.addConsumption = async function (userId, conso) {
    const timestamp = Date.now()
    return await consoRepository.addConsumption(timestamp, conso, userId)
}

exports.getConsumptionHistory = async function (userId, month, year) {
    console.log('Getting consumption history of user ' + userId)
    let res = {}
    if (month !== undefined) {
        if (isNaN(month) || month <= 0 || month > 12) {
            res.error = "Month is not an int, is inferior to 0 or superior to 12. Month is : " + month
            console.error(res.error)
            return res
        }
        if (year !== undefined) {
            if (isNaN(year) || year < 2000) {
                res.error = "Year is not an int or is inferior to year 2000. Year is : " + year
                console.error(res.error)
                return res
            }
            return await consoRepository.getConsumptionByMonthAndYear(userId, month, year)
        }
        return await consoRepository.getConsumptionByMonth(userId, month)
    }
    if (year !== undefined) {
        if (isNaN(year) || year < 2000) {
            res.error = "Year is not an int or is inferior to year 2000. Year is : " + year
            console.error(res.error)
            return res
        }
        return await consoRepository.getConsumptionByYear(userId, year)
    }
    return await consoRepository.getConsumption(userId)

}
