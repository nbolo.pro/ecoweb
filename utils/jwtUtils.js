var exports = module.exports = {}
const jwt = require('jsonwebtoken')
const jwtSecret = require('../utils/constUtils.js').jwtsecret
const JWT_EXPIRE_TIME = require('../utils/constUtils.js').jwtexpires

exports.generateToken = function (userId, name, surname, mail) {
    let res = {}
    const payload = {
        userId: userId,
        name: name,
        surname: surname,
        mail: mail,
        exp: Math.floor(new Date(Date.now() + JWT_EXPIRE_TIME).getTime() / 1000)
    }

    res.payload = payload
    res.token = jwt.sign(payload, jwtSecret)

    return res
}
